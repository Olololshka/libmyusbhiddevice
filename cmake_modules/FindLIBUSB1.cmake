# - Try to find libsettingscmdline
# Once done this will define
#
#  LIBUSB_FOUND - system has SETTINGSCMDLINE
#  LIBUSB_INCLUDE_DIR - the SETTINGSCMDLINE include directory
#  LIBUSB_LIBRARIES - Link these to use SETTINGSCMDLINE

# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (LIBUSB_INCLUDE_DIR AND LIBUSB_LIBRARIES)

  # in cache already
  set(LIBUSB_FOUND TRUE)
  message(STATUS "Found libsettingscmdline: ${LIBUSB_LIBRARIES}")

else (LIBUSB_INCLUDE_DIR AND LIBUSB_LIBRARIES)

  find_path(LIBUSB_INCLUDE_DIR 
	NAMES libusb.h
	PATHS 
		/usr/include
		/usr/include/libusb-1.0
		/usr/local/include
		$ENV{WD}/../include
		$ENV{WD}/../local/include
 )
 
 find_library(LIBUSB_LIBRARIES 
	NAME usb-1.0
    PATHS
		/usr/lib
		/usr/bin
		/usr/local/lib
		/usr/local/bin
  )

 set(CMAKE_REQUIRED_INCLUDES ${LIBUSB_INCLUDE_DIR})
 set(CMAKE_REQUIRED_LIBRARIES ${LIBUSB_LIBRARIES})

   if(LIBUSB_INCLUDE_DIR AND LIBUSB_LIBRARIES)
    set(LIBUSB_FOUND TRUE)
  else (LIBUSB_INCLUDE_DIR AND LIBUSB_LIBRARIES)
    set(LIBUSB_FOUND FALSE)
  endif(LIBUSB_INCLUDE_DIR AND LIBUSB_LIBRARIES)

  if (LIBUSB_FOUND)
    if (NOT LIBUSB_FIND_QUIETLY)
      message(STATUS "Found libusb-1.0: ${LIBUSB_LIBRARIES}")
    endif (NOT LIBUSB_FIND_QUIETLY)
  else (LIBUSB_FOUND)
    if (LIBUSB_FIND_REQUIRED)
      message(FATAL_ERROR "libusb-1.0 not found. Please install it.")
    endif (LIBUSB_FIND_REQUIRED)
  endif (LIBUSB_FOUND)

  mark_as_advanced(LIBUSB_INCLUDE_DIR LIBUSB_LIBRARIES)

endif (LIBUSB_INCLUDE_DIR AND LIBUSB_LIBRARIES)
