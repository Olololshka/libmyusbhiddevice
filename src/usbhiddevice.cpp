#include <stdexcept>

#include <QTimerEvent>

#include <libusb.h>

#include <mylog.h>

#include "usbhiddevice.h"

static const int HID_GET_REPORT = 0x01;
static const int HID_SET_REPORT = 0x09;
static const int HID_REPORT_TYPE_INPUT = 0x01;
static const int HID_REPORT_TYPE_OUTPUT = 0x02;
static const int HID_REPORT_TYPE_FEATURE = 0x03;

static const int CONTROL_REQUEST_TYPE_IN = LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE;
static const int CONTROL_REQUEST_TYPE_OUT = LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE;

struct libusb_transfer * UsbHidDevice::interruptIN_transfer;

UsbHidDevice::UsbHidDevice(uint16_t vid, uint16_t pid, QObject *parent) :
    QIODevice(parent)
{
    VID = vid;
    PID = pid;

    reportData = NULL;

    int result = libusb_init(NULL);
    if (result != 0)
        throw std::runtime_error("libusb init failed");

    usbPooler = new pooler(this);
}

UsbHidDevice::~UsbHidDevice()
{
    close();
    libusb_exit(NULL);
}

bool UsbHidDevice::canReadLine() const
{
    return false;
}

void UsbHidDevice::close()
{
    usbPooler->terminate();

    if (devh)
    {
        libusb_release_interface(devh, INTERFACE_NUMBER);
        libusb_attach_kernel_driver(devh, INTERFACE_NUMBER);
        libusb_close(devh);
    }

    QIODevice::close();
}

bool UsbHidDevice::isSequential() const
{
    return false;
}

bool UsbHidDevice::open(QIODevice::OpenMode mode)
{
    devh = libusb_open_device_with_vid_pid(NULL, VID, PID);
    if  (devh == NULL)
        return false;
    libusb_detach_kernel_driver(devh, INTERFACE_NUMBER);
    int result = libusb_claim_interface(devh, INTERFACE_NUMBER);
    if (result != LIBUSB_SUCCESS)
        return false;

    if (!readReportSizes())
        return false;

    allocUsbInterruptINTransfer(maxInterruptInreportSize);
    setPoolInterval(DEFAULT_POOL_INTERVAL);

    usbPooler->start();

    return QIODevice::open(mode);
}

bool UsbHidDevice::reset()
{
    return seek(0);
}

bool UsbHidDevice::seek(qint64 pos)
{
    if (QIODevice::seek(pos))
    {
        _pointer p =
        {
            (uint8_t)(pos >> 16), (uint16_t)pos
        };
        int res = libusb_control_transfer(
                    devh,
                    CONTROL_REQUEST_TYPE_OUT,
                    HID_SET_REPORT,
                    (HID_REPORT_TYPE_OUTPUT<<8)|0x00,
                    INTERFACE_NUMBER,
                    (unsigned char*)&p,
                    sizeof(p),
                    DEFAULT_USB_TIMEOUT
                    );
        return (res == sizeof(_pointer));
    }
    else
        return false;
}

qint64 UsbHidDevice::bytesAvailable() const
{
    return maxTraansferBy1Ctlreport - 1;
}

void UsbHidDevice::setPoolInterval(int poolInterval)
{
    usbPooler->setPoolInterval(poolInterval);
}

qint64 UsbHidDevice::readData(char *data, qint64 maxSize)
{
    qint64 bytesTransferted = 0;
    for (;maxSize;)
    {
        uint bytes_to_transfer = (maxSize > (maxTraansferBy1Ctlreport - 1)) ?
                    maxTraansferBy1Ctlreport - 1 : maxSize;
        unsigned char buff[maxTraansferBy1Ctlreport];
        uint bytes_received = libusb_control_transfer(
                    devh,
                    CONTROL_REQUEST_TYPE_IN,
                    HID_GET_REPORT,
                    (HID_REPORT_TYPE_FEATURE << 8)|0x00,
                    INTERFACE_NUMBER,
                    buff,
                    maxTraansferBy1Ctlreport,
                    DEFAULT_USB_TIMEOUT);

        if ((bytes_received != maxTraansferBy1Ctlreport))
            break; //error

        int bytes_received_realy = qMin((uint)buff[0], bytes_to_transfer); //realy ressived
        memcpy(data + bytesTransferted, buff + 1, bytes_received_realy);

        bytesTransferted += bytes_received_realy;

        maxSize -= bytes_received_realy;
    }
    return bytesTransferted;
}

qint64 UsbHidDevice::writeData(const char *data, qint64 maxSize)
{
    qint64 readpos = 0;
    for (; readpos < maxSize;)
    {
        int bytes_to_transfer = (maxSize - readpos > (maxTraansferBy1Ctlreport - 1)) ?
                    (maxTraansferBy1Ctlreport - 1) : maxSize - readpos;
        unsigned char curentTransfer[maxTraansferBy1Ctlreport];
        curentTransfer[0] = bytes_to_transfer;
        memcpy(curentTransfer + 1, data + readpos, bytes_to_transfer);

        int bytes_sent = libusb_control_transfer(
                    devh,
                    CONTROL_REQUEST_TYPE_OUT,
                    HID_SET_REPORT,
                    (HID_REPORT_TYPE_FEATURE << 8)|0x00,
                    INTERFACE_NUMBER,
                    curentTransfer,
                    maxTraansferBy1Ctlreport,
                    DEFAULT_USB_TIMEOUT);
        if (bytes_sent != maxTraansferBy1Ctlreport)
            return -1; //error

        readpos += bytes_to_transfer;
    }
    return readpos;
}

void UsbHidDevice::LIBUSB_CALLBCK_transfer(libusb_transfer *transfer)
{
    UsbHidDevice* _this = (UsbHidDevice*)transfer->user_data;

    switch(transfer->status)
    {
    case LIBUSB_TRANSFER_COMPLETED:
        emit _this->IntDataRessived(QByteArray((const char*)transfer->buffer, transfer->length));
        break;

    case LIBUSB_TRANSFER_TIMED_OUT:
        //LOG_DEBUG("Interrupt transfer timeout."); // скорее всего LUFA не отправила репорт, ибо он такой-же как предидущий
        break;

    case LIBUSB_TRANSFER_ERROR:
    case LIBUSB_TRANSFER_CANCELLED:
        LOG_INFO(trUtf8("USB error detected: %1").arg(transfer->status));
        emit _this->USB_error(transfer->status);
        break;

    case LIBUSB_TRANSFER_STALL: // у устройства нет данных
        break;

    case LIBUSB_TRANSFER_NO_DEVICE:
    case LIBUSB_TRANSFER_OVERFLOW:
        LOG_ERROR(trUtf8("Uncorrectable USB error detected: %1").arg(transfer->status));
        libusb_free_transfer(interruptIN_transfer);
        emit _this->USB_error(transfer->status);
        return;
    }

    int res = libusb_submit_transfer(interruptIN_transfer);
    if (res)
    {
        libusb_free_transfer(interruptIN_transfer);
        emit _this->USB_error((libusb_transfer_status)-1);
    }
}

void UsbHidDevice::allocUsbInterruptINTransfer(int interruptInReportSize)
{
    Q_ASSERT(interruptInReportSize >= 0);
    reportData = (unsigned char*)malloc(interruptInReportSize);
    if (reportData == NULL)
        throw std::overflow_error("failed to alocate resive buffer");
    interruptIN_transfer = libusb_alloc_transfer(0);
    if (!interruptIN_transfer)
        throw std::overflow_error("Can't alocate interupt transfer.");
    libusb_fill_interrupt_transfer(interruptIN_transfer,
                                   devh,
                                   INTERRUPT_IN_ENDPOINT,
                                   reportData,
                                   interruptInReportSize,
                                   (libusb_transfer_cb_fn)LIBUSB_CALLBCK_transfer,
                                   this,
                                   DEFAULT_USB_TIMEOUT
                                   );
    int res = libusb_submit_transfer(interruptIN_transfer);
    if (res)
    {
        libusb_free_transfer(interruptIN_transfer);
        throw std::runtime_error("failed to submit transfer");
    }
}

bool UsbHidDevice::readReportSizes()
{
    QByteArray result(256, Qt::Uninitialized);
    int res = libusb_get_descriptor(
                devh,
                LIBUSB_DT_REPORT,
                0,
                (unsigned char*)result.data(),
                result.size()
                );
    if (res <= 0)
        return false;
    result.remove(res, result.size() - res);

    // принимаемый диспкиптор почему-то не совсем соответствует объявленому,
    //поэтому тут небольшой костылик
    int index = result.lastIndexOf(0x95);
    if (index < 1)
        return false; // parce error
    else
        maxTraansferBy1Ctlreport = result.at(index + 1);
    index = result.indexOf(0x95);
    if (index < 1)
        return false; // parce error
    else
        maxInterruptInreportSize = result.at(index + 1);
    return true; //sucess
}

UsbHidDevice::pooler::pooler(UsbHidDevice *parent):
    QThread(parent)
{
    setPoolInterval(DEFAULT_POOL_INTERVAL);
}

void UsbHidDevice::pooler::run()
{
    while(1)
    {
        int sleepinterval;

        mutex.lock();
        sleepinterval = PoolInterval;
        mutex.unlock();

        libusb_handle_events(NULL);

        msleep(sleepinterval);
    }
}

void UsbHidDevice::pooler::setPoolInterval(int interval)
{
    mutex.lock();
    PoolInterval = interval;
    mutex.unlock();
}
