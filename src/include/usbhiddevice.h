#ifndef USBHIDDEVICE_H
#define USBHIDDEVICE_H

#include <QIODevice>
#include <QMutex>
#include <QThread>
#include <stdint.h>

#define INTERFACE_NUMBER            (0)
#define INTERRUPT_IN_ENDPOINT       (0x81)
#define DEFAULT_USB_TIMEOUT         (100)
#define DEFAULT_POOL_INTERVAL       (10)

struct libusb_transfer;
struct libusb_device_handle;

class UsbHidDevice : public QIODevice
{
    Q_OBJECT
public:
    explicit UsbHidDevice(uint16_t vid, uint16_t pid, QObject *parent = 0);
    virtual ~UsbHidDevice();

    virtual bool 	canReadLine () const; // вернуть false (строк не читаем)
    virtual void 	close ();
    virtual bool 	isSequential () const; //вернуть fase
    virtual bool 	open ( OpenMode mode );
    virtual bool 	reset (); // сбросить указатель в начало
    virtual bool 	seek ( qint64 pos ); // установка указателя
    virtual qint64 	bytesAvailable () const; // количество доступных байт для чтения

public slots:
    void setPoolInterval(int poolInterval);

protected:
    virtual qint64 	readData ( char * data, qint64 maxSize );
    virtual qint64 	writeData ( const char * data, qint64 maxSize );

signals:
    void IntDataRessived(const QByteArray& outdata);
    void USB_error(const int err);

private:
    struct _pointer
    {
        uint8_t	PointerType;
        uint16_t Offset;
    }__attribute__((packed));

    static struct libusb_transfer * interruptIN_transfer;
    struct libusb_device_handle *devh;
    unsigned char* reportData;
    int maxTraansferBy1Ctlreport, maxInterruptInreportSize;

    uint16_t VID, PID;

    static void LIBUSB_CALLBCK_transfer(struct libusb_transfer *transfer);

    class pooler : public QThread
    {
    public:
        explicit pooler(UsbHidDevice* parent);
        void setPoolInterval(int interval);
    protected:
        virtual void run();

    private:
        QMutex mutex;
        int PoolInterval;
    };

    friend class pooler;

    pooler* usbPooler;

protected:
    virtual void allocUsbInterruptINTransfer(int interruptInReportSize);
    bool readReportSizes();
};

#endif // USBHIDDEVICE_H
